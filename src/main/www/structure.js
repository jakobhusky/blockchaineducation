var edges = {
    "home": [
        "general",
        "cryptography",
        "cryptocurrency",
        "development",
        "about"
    ],

    "general": [
        "revolution",
        "public-vs-private",
        "use-cases",
        "flaws"
    ],
    "cryptography": [
        "hashing",
        "public-key-cryptography",
        "consensus"
    ],
    "cryptocurrency": [
        "mining-101",
        "bitcoin-101",
        "etherium-101",
        "stellar-101"
    ],
    "development": [
        "development-intro",
        "smart-contract-development",
        "hyperledger"
    ],
    "about": [
        "bem-intro"
    ],

    "revolution": [],
    "public-vs-private": [],
    "use-cases": [],
    "flaws": [],

    "hashing": [],
    "public-key-cryptography": [],
    "consensus": [
        "consensus-intro",
        "proof-of-work",
        "alternative-consensus",
        "smart-contracts"
    ],

    "mining-101": [
        "mining-intro",
        "mining-rewards",
        "mining-hardware"
    ],
    "bitcoin-101": [
        "bitcoin-history",
        "bitcoin-protocol",
        "bitcoin-transactions",
        "bitcoin-howto",
        "altcoins"
    ],
    "etherium-101": [
        "scaling-solution",
        "smart-contracts"
    ],
    "stellar-101": [
        "devia",
        "hosting-a-node"
    ],

    "development-intro": [],
    "smart-contract-development": [],
    "hyperledger": [],

    "bem-intro": [],

    "consensus-intro": [],
    "proof-of-work": [],
    "alternative-consensus": [],

    "mining-intro": [],
    "mining-rewards": [],
    "mining-hardware": [],

    "bitcoin-history": [],
    "bitcoin-protocol": [],
    "bitcoin-transactions": [],
    "bitcoin-howto": [],
    "altcoins": [],

    "scaling-solution": [
        "plasma",
        "state-channels",
        "sharding"
    ],
    "smart-contracts": [],

    "devia": [],
    "hosting-a-node": [],

    "plasma": [],
    "state-channels": [],
    "sharding": []
};
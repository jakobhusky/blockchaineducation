package blockchainEducation

import scala.scalajs.js
import scala.scalajs.js.annotation.JSGlobalScope

@js.native
@JSGlobalScope
object Globals extends js.Any {
  val edges: js.Dictionary[js.Array[String]] = js.native
}

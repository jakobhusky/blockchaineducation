package blockchainEducation.controller

import blockchainEducation.graph._
import blockchainEducation.ui.{Circle, CircleEdge}
import blockchainEducation.util.Vec
import org.querki.jquery._
import org.scalajs.dom
import org.scalajs.dom.raw.HTMLDivElement

class GraphController(val graph: Graph[Circle, CircleEdge],
                      val containerElem: dom.Element,
                      val didSelectNode: () => Unit,
                      initNode: Node[Circle, CircleEdge]) {
  private var _currentNode: Node[Circle, CircleEdge] = initNode

  def currentNode: Node[Circle, CircleEdge] = _currentNode

  def resize(): Unit = {
    deconfigure()
    setup()
  }

  private def setup(): Unit = {
    graph.nodes.idxSeq.foreach(setupNode)
  }

  private def deconfigure(): Unit = {
    graph.nodes.idxSeq.foreach(deconfigureNode)
  }

  private def setupNode(node: Node[Circle, CircleEdge]): Unit = {
    val containerSize =
      Vec(containerElem.clientWidth, containerElem.clientHeight)
    val containerCenter = Vec(containerSize.x / 2, containerSize.y / 2)
    val elem = node.content.elem

    val (nodeClass: String,
         scale: Double,
         clickHandler: Option[() => Unit @unchecked]) = {
      if (node.id == currentNode.id) {
        (
          "main",
          Circle.mainScale,
          None
        )
      } else {
        node.inEdges
          .find { _.id.from == currentNode.id }
          .map { edge =>
            (
              "child",
              Circle.outerScale,
              Some(() => selectChild(node))
            )
          }
          .orElse(node.outEdges.find { _.id.to == currentNode.id }.map { edge =>
            (
              "parent",
              Circle.outerScale,
              Some(() => selectParent(node))
            )
          })
          .getOrElse {
            if (node.id.tag == "home") {
              ("home-ancestor",
               Circle.homeAncestorScale,
               Some(() => selectHomeAncestor(node)))
            } else {
              ("offscreen", 0, None)
            }
          }
      }
    }
    elem.classList.add(nodeClass)

    val maxDistance = {
      if (node.id.tag == "home") {
        Int.MaxValue
      } else {
        2
      }
    }
    val centerOffset = graph.pathBetween(currentNode, node, maxDistance) match {
      case Some(path) =>
        if (path.isEmpty) {
          Vec.zero
        } else {
          val pathSizeMult = {
            if (path.edges.length > 1) {
              1.25
            } else {
              1.0
            }
          }

          path.edges
            .map { biEdge =>
              val edgeDistance = CircleEdge.distance(
                containerSize,
                currentNode.content.elem.classList.contains("big-main"))
              val edgeDirection = biEdge.direction match {
                case Forward  => biEdge.edge.content.outDirection
                case Backward => biEdge.edge.content.inDirection
              }
              Vec.radial(
                edgeDistance,
                edgeDirection
              )
            }
            .reduce(_ + _) / path.edges.size * pathSizeMult
        }
      case None => Vec.zero //Too far away to care
    }
    val center = containerCenter + centerOffset

    def reposition(scale: Double): Unit = {
      val height = containerSize.minDim * scale
      val width: Double = {
        if (elem.classList.contains("main") && elem.classList.contains(
              "big-main")) {
          height * Circle.bigMainWidthMult
        } else {
          height
        }
      }
      elem.style.width = width + "px"
      elem.style.height = height + "px"
      elem.style.left = (center.x - width / 2) + "px"
      elem.style.top = (center.y - height / 2) + "px"
    }

    if (nodeClass != "main") {
      $(elem).on("mouseenter.setup", { () =>
        reposition(scale * Circle.hoverScaleMult)
      })
      $(elem).on("mouseleave.setup", { () =>
        reposition(scale)
      })
    }
    clickHandler.foreach { clickHandler =>
      $(elem).on("click.setup", { () =>
        clickHandler()
      })
    }

    reposition(scale)
    didSelectNode()
  }

  private def deconfigureNode(node: Node[Circle, CircleEdge]): Unit = {
    val elem = node.content.elem

    elem.classList.remove("main")
    elem.classList.remove("child")
    elem.classList.remove("parent")
    elem.classList.remove("home-ancestor")
    elem.classList.remove("offscreen")
    $(elem).unbind(".setup")
  }

  private def selectChild(node: Node[Circle, CircleEdge]): Unit = {
    deconfigure()
    _currentNode = node
    setup()
  }

  private def selectParent(node: Node[Circle, CircleEdge]): Unit = {
    deconfigure()
    _currentNode = node
    setup()
  }

  private def selectHomeAncestor(node: Node[Circle, CircleEdge]): Unit = {
    deconfigure()
    _currentNode = node
    setup()
  }

  setup()
}

object GraphController {
  def create(nodes: Traversable[dom.Element],
             edges: Traversable[(String, Seq[String])],
             containerElem: dom.Element,
             didSelectNode: () => Unit,
             customInitTag: String): GraphController = {
    val graph = Graph(
      nodes.map { nodeElem =>
        val nodeDiv = nodeElem.asInstanceOf[HTMLDivElement]
        nodeDiv.classList.add("node")
        val nodeId = nodeDiv.id
        val nodeCircle = Circle(nodeDiv)
        (nodeId, nodeCircle)
      }.toSeq,
      edges.flatMap {
        case (fromTag, toTags) =>
          toTags.zipWithIndex.map {
            case (toTag, idx) =>
              ((fromTag, toTag), CircleEdge(idx, toTags.size))
          }
      }.toMap
    )
    new GraphController(
      graph,
      containerElem,
      didSelectNode,
      initNode = graph.nodes.tagMap
        .getOrElse(customInitTag, graph.nodes.tagMap("home")),
    )
  }
}

package blockchainEducation.graph

case class NodeCol[T, E](tagMap: Map[String, Node[T, E]], idxSeq: Seq[Node[T, E]], size: Int) {
  val contents: Seq[T] = idxSeq.map(_.content)

  def byId(id: NodeId): Node[T, E] = idxSeq(id.index)

  def idByTag(tag: String): NodeId = tagMap(tag).id
}

object NodeCol {
  def apply[T, E](taggedNodes: Seq[(String, T)], edges: Set[Edge[E]]): NodeCol[T, E] = {
    val iddNodes = taggedNodes.zipWithIndex.map {
      case ((tag, content), idx) =>
        val id = NodeId(tag, idx)
        (id, Node(
          content,
          id,
          inEdges = edges.filter(_.id.to == id),
          outEdges = edges.filter(_.id.from == id)
        ))
    }
    NodeCol(
      tagMap = iddNodes.map {
        case (id, node) => (id.tag, node)
      }.toMap,
      idxSeq = iddNodes.map(_._2),
      size = iddNodes.size
    )
  }
}

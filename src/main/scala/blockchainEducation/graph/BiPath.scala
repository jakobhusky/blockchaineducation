package blockchainEducation.graph

case class BiPath[T](edges: List[BiEdge[T]]) {
  val isEmpty: Boolean = edges.isEmpty

  def ::(edge: BiEdge[T]): BiPath[T] = BiPath(edge :: edges)
}

object BiPath {
  def empty[T]: BiPath[T] = BiPath(List.empty)
}
package blockchainEducation.graph

case class Node[T, E](content: T, id: NodeId, inEdges: Set[Edge[E]], outEdges: Set[Edge[E]]) {

}

object Node {

}
package blockchainEducation.graph

case class EdgeCol[T](map: Map[EdgeId, T], array: Seq[Seq[Option[T]]], size: Int) {

}

object EdgeCol {
  def apply[T](map: Map[EdgeId, T], nodeCount: Int, nodeTags: Seq[String]): EdgeCol[T] = {
    val nodeIds = Seq.tabulate(nodeCount)(i => NodeId(nodeTags(i), i))
    new EdgeCol(
      map,
      Seq.tabulate(nodeCount, nodeCount) { (i, j) =>
        val edge = EdgeId(from = nodeIds(i), to = nodeIds(j))
        map.get(edge)
      },
      map.size
    )
  }
}
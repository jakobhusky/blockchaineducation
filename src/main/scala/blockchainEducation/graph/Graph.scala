package blockchainEducation.graph

case class Graph[N, E](nodes: NodeCol[N, E], edges: EdgeCol[E]) {
  def pathBetween(start: Node[N, E], end: Node[N, E], maxDistance: Int): Option[BiPath[E]] = {
    _pathBetween1(start, end, maxDistance, minDistance = 0)
  }

  private def _pathBetween1(start: Node[N, E], end: Node[N, E], maxDistance: Int, minDistance: Int): Option[BiPath[E]] = {
    if (maxDistance < minDistance) {
      None
    } else {
      _pathBetween2(start, end, distance = minDistance, avoid = Set.empty).orElse {
        _pathBetween1(start, end, maxDistance, minDistance = minDistance + 1)
      }
    }
  }

  private def _pathBetween2(start: Node[N, E], end: Node[N, E], distance: Int, avoid: Set[NodeId]): Option[BiPath[E]] = {
    if (distance == 0) {
      if (start.id == end.id) {
        Some(BiPath.empty)
      } else {
        None
      }
    } else {
      if (avoid.contains(start.id)) {
        None
      } else {
        start.outEdges.map { outEdge =>
          val outNode = nodes.byId(outEdge.id.to)
          _pathBetween2(outNode, end, distance = distance - 1, avoid + start.id).map {
            BiEdge(outEdge, Forward) :: _
          }
        }.collectFirst { case Some(x) => x }.orElse {
          start.inEdges.map { inEdge =>
            val inNode = nodes.byId(inEdge.id.from)
            _pathBetween2(inNode, end, distance = distance - 1, avoid + start.id).map {
              BiEdge(inEdge, Backward) :: _
            }
          }.collectFirst { case Some(x) => x }
        }
      }
    }
  }
}

object Graph {
  def apply[N, E](taggedNodes: Seq[(String, N)], tagEdgeMap: Map[(String, String), E]): Graph[N, E] = {
    val tagToIdx = taggedNodes.map(_._1).zipWithIndex.toMap
    val edgeMap = tagEdgeMap.map {
      case ((fromTag, toTag), edgeContent) => (EdgeId(
        from = NodeId(fromTag, tagToIdx(fromTag)),
        to = NodeId(toTag, tagToIdx(toTag))
      ), edgeContent)
    }

    Graph(
      nodes = NodeCol(
        taggedNodes,
        edges = edgeMap.map {
          case (id, content) => Edge(content, id)
        }.toSet
      ),
      edges = EdgeCol(
        map = edgeMap,
        nodeCount = taggedNodes.size,
        nodeTags = taggedNodes.map(_._1)
      )
    )
  }
}
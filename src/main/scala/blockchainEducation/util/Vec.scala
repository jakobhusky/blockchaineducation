package blockchainEducation.util

case class Vec(x: Int, y: Int) {
  val minDim: Int = Math.min(x, y)

  def unary_- : Vec = {
    Vec(-x, -y)
  }

  def +(other: Vec): Vec = {
    Vec(x + other.x, y + other.y)
  }

  def -(other: Vec): Vec = {
    Vec(x - other.x, y - other.y)
  }

  def *(scale: Double): Vec = {
    Vec(x = Math.round(x * scale).toInt, y = Math.round(y * scale).toInt)
  }

  def /(scale: Double): Vec = {
    Vec(x = Math.round(x / scale).toInt, y = Math.round(y / scale).toInt)
  }
}

object Vec {
  val zero: Vec = Vec(x = 0, y = 0)

  def radial(magnitude: Double, direction: Double): Vec = {
    print(direction * (180 / Math.PI))
    Vec(
      x = Math.round(Math.sin(direction) * magnitude).toInt,
      y = Math.round(Math.cos(direction) * magnitude).toInt
    )
  }
}

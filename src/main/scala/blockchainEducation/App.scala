package blockchainEducation

import blockchainEducation.controller.GraphController
import org.scalajs.dom
import org.scalajs.dom.ext._

object App {
  var graph: GraphController = _

  def main(): Unit = {
    org.scalajs.dom.console.log("JavaScript working")
    dom.window.onload = { _ =>
      setup()
    }
  }

  private def setup(): Unit = {
    setupGraph()
    fixIframes()
  }

  private def setupGraph(): Unit = {
    val containerElem = dom.document.getElementById("nodes")
    graph = GraphController.create(
      nodes = containerElem.children.toTraversable,
      edges = Globals.edges.toMap.mapValues(_.toSeq),
      containerElem = containerElem,
      didSelectNode = { () =>
        if (graph != null) {
          updateForCurrentNode()
        }
      },
      customInitTag = dom.window.location.hash.stripPrefix("#")
    )
    updateForCurrentNode()
    dom.window.onresize = { _ =>
      graph.resize()
    }
    org.scalajs.dom.console.log("Setup graph")
  }

  private def updateForCurrentNode(): Unit = {
    dom.window.location.hash = graph.currentNode.id.tag
  }

  private def fixIframes(): Unit = {
    //From https://stackoverflow.com/questions/3253362/iframe-src-caching-issue-on-firefox
    dom.document.getElementsByTagName("iframe").foreach { node =>
      val iFrame = node.asInstanceOf[dom.html.IFrame]
      iFrame.src = iFrame.src
    }
  }
}

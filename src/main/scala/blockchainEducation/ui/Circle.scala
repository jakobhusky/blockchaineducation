package blockchainEducation.ui

import blockchainEducation.util.Vec
import org.scalajs.dom.raw.HTMLDivElement

case class Circle(elem: HTMLDivElement) {}

object Circle {
  val mainScale: Double = 0.5
  val outerScale: Double = 0.2
  val homeAncestorScale: Double = 0.1
  val hoverScaleMult: Double = 1.25
  val bigMainWidthMult: Double = 1.5

  def homeAncestorPos(containerSize: Vec): Vec = {
    val cornerOffset = Math.round(containerSize.minDim * 0.1).toInt
    Vec(
      x = containerSize.x - cornerOffset,
      y = cornerOffset
    )
  }
}

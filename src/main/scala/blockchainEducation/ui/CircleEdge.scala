package blockchainEducation.ui

import blockchainEducation.util.Vec

case class CircleEdge(index: Int, groupCount: Int) {
  val inDirection: Double = {
    Math.PI - (((groupCount / 2.0) - index - 0.5) * CircleEdge.groupSepAngle)
  }

  val outDirection: Double = {
    inDirection - Math.PI
  }
}

object CircleEdge {
  private val groupSepAngle: Double = Math.PI / 5
  private val distanceMultiplier: Double = 0.35
  private val bigMainEdgeDistMult: Double = 1.25

  def distance(containerSize: Vec, isMainBig: Boolean): Double = {
    val baseDistance = containerSize.minDim * distanceMultiplier
    if (isMainBig) {
      baseDistance * bigMainEdgeDistMult
    } else {
      baseDistance
    }
  }
}

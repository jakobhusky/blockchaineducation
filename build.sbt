enablePlugins(ScalaJSPlugin)

name := "BlockchainEducation"

version := "1.0.0"

scalaVersion := "2.12.4"

libraryDependencies ++= Seq(
  "org.scala-lang" % "scala-library" % "2.12.4",
  "org.slf4j" % "slf4j-api" % "1.7.19",
  "org.scala-js" %%% "scalajs-dom" % "0.9.2",
  "org.querki" %%% "jquery-facade" % "1.2",
  "junit" % "junit" % "4.12"
)
jsDependencies ++= Seq(
  "org.webjars" % "jquery" % "2.2.1" / "jquery.js" minified "jquery.min.js"
)

scalacOptions in Test ++= Seq("-Yrangepos")

scalaJSUseMainModuleInitializer := true

mainClass in Compile := Some("blockchainEducation.App")
